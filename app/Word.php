<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Word extends Model
{
    protected $fillable = ['word'];

    public function getWordShuffledAttribute()
    {
        return str_shuffle($this->word);
    }
}
