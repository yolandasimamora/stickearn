@extends('layout')

@section('body')
    <div class="head-title">
        <h1><a href="#">Scramble Words</a></h1>
    </div>
    <div class="login-form">
        @if (! empty(session('message')))
            {{ session('message') }}
        @endif
        <form action="{{ route('auth.postLogin') }}" method="POST">
            @csrf
            <input type="text" name="email" placeholder="email" value="{{ old('email') }}" required="required"><br>
            <input type="password" name="password" placeholder="password" required="required"><br>
            <button type="submit" class="btn-login">Lets Play</button>
        </form>
        <a href="{{ route('auth.register') }}" class="register">Register</a>
    </div>
@endsection