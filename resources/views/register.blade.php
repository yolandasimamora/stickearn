@extends('layout')

@section('body')
    <div class="head-title">
        <h1><a href="#">Scramble Words</a></h1>
    </div>
    <div class="login-form">
        @if (! empty(session('message')))
            {{ session('message') }}
        @endif
        @if (! empty($errors))
            {{ $errors->first() }}
        @endif
        <form action="{{ route('auth.postRegister') }}" method="POST">
            @csrf
            <input type="text" name="name" placeholder="name" value="{{ old('name') }}" required="required"><br>
            <input type="text" name="email" placeholder="email" value="{{ old('email') }}" required="required"><br>
            <input type="password" name="password" placeholder="password" required="required"><br>
            <button type="submit" class="btn-login">Register</button>
        </form>
        <a href="{{ route('auth.login') }}" class="register">Already have account</a>
    </div>
@endsection