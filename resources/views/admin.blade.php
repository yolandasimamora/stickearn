@extends('layout')

@section('body')
    <div class="header">
        <button type="button" class="btn-logout">
            X
        </button>
    </div>

    <div class="table-responsive">
        <table class="table">
            <tr style='text-align: center;'>
                <th>Name</th>
                <th>Points</th>
                <th>Time</th>
            </tr>
            @forelse($histories as $history)
            <tr style='text-align: center;'>
                <td>{{ $history->user->name }}</td>
                <td class="text-center">{{ $history->point > 0 ? '+'.$history->point : $history->point }}</td>
                <td class="text-center">{{ $history->created_at->diffForHumans() }}</td>
            </tr>
            @empty
            <tr style='text-align: center;'>
                <td colspan="3">no data available</td>
            </tr>
            @endforelse
        </table>
        <div class ="pagination" style="position: right;">
            {{ $histories->links() }}
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>
        $(function() {
                $('.btn-logout').click(function() {
                window.location.replace('/logout');
            });
        })
    </script>
@endsection