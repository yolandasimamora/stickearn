<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WordsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('words')->insert(['word' => 'spinach', 'created_at' => now()]);
        DB::table('words')->insert(['word' => 'cheese', 'created_at' => now()]);
        DB::table('words')->insert(['word' => 'peanut', 'created_at' => now()]);
        DB::table('words')->insert(['word' => 'rice', 'created_at' => now()]);
        DB::table('words')->insert(['word' => 'chocolate', 'created_at' => now()]);
        DB::table('words')->insert(['word' => 'coconut', 'created_at' => now()]);
        DB::table('words')->insert(['word' => 'ketchup', 'created_at' => now()]);
        DB::table('words')->insert(['word' => 'banana', 'created_at' => now()]);
        DB::table('words')->insert(['word' => 'apple', 'created_at' => now()]);
        DB::table('words')->insert(['word' => 'pear', 'created_at' => now()]);
        DB::table('words')->insert(['word' => 'strawberry', 'created_at' => now()]);
        DB::table('words')->insert(['word' => 'grapes', 'created_at' => now()]);
        DB::table('words')->insert(['word' => 'blueberry', 'created_at' => now()]);
        DB::table('words')->insert(['word' => 'guava', 'created_at' => now()]);
        DB::table('words')->insert(['word' => 'rambutan', 'created_at' => now()]);
        DB::table('words')->insert(['word' => 'papaya', 'created_at' => now()]);
        DB::table('words')->insert(['word' => 'watermelon', 'created_at' => now()]);
        DB::table('words')->insert(['word' => 'cauliflower', 'created_at' => now()]);
        DB::table('words')->insert(['word' => 'coriander', 'created_at' => now()]);
        DB::table('words')->insert(['word' => 'soup', 'created_at' => now()]);
        DB::table('words')->insert(['word' => 'carrot', 'created_at' => now()]);
        DB::table('words')->insert(['word' => 'steak', 'created_at' => now()]);
        DB::table('words')->insert(['word' => 'peas', 'created_at' => now()]);
        DB::table('words')->insert(['word' => 'cucumber', 'created_at' => now()]);
        DB::table('words')->insert(['word' => 'mango', 'created_at' => now()]);
        DB::table('words')->insert(['word' => 'milk', 'created_at' => now()]);
        DB::table('words')->insert(['word' => 'lemon', 'created_at' => now()]);
    }
}
